const findFunction = (array,cbFunction) => {
let valueFound;
    for(let index=0;index<array.length;index++){
       valueFound = cbFunction(array[index],index,array);
       //console.log(valueFound);
       if(valueFound===true){
        return array[index];
       }
    
    }
    return valueFound;
}

module.exports = findFunction;