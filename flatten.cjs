const flattenFunction = (array,depth) => { 
  
    const flattenArray = [];
    if(depth===undefined){
        depth=1;
    }

    const flatten = (arr,currentDepth) => {
        for (let index = 0; index < arr.length; index++) {
            if(index in array){
                if (Array.isArray(arr[index]) && currentDepth > 0) {
                    flatten(arr[index], currentDepth - 1);
                  } else {
                    flattenArray.push(arr[index]);
                  }
            }
         
        }
      };

    flatten(array,depth);
    return flattenArray;
    };


module.exports = flattenFunction;
