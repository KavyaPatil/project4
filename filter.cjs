const filterFunction = (array, cbFunction) => {

  let filteredElements = [];
  
  for (let index = 0; index < array.length; index++) {
    let elementFiltered = cbFunction(array[index],index,array);
    if (elementFiltered === true) {
      filteredElements.push(array[index]);
    }
  }
  return filteredElements;
};

module.exports = filterFunction;
