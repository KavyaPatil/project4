const testArray = require('../array.cjs');
const testFindFunction = require('../find.cjs');

const callBackFunction = (element) => {
    if(element === 5){
        return true;
    }
}

const result = testFindFunction(testArray,callBackFunction);

console.log(result)