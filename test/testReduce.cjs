const testArray = require("../array.cjs");
const testReduceFunction = require("../reduce.cjs");
// const initialValue =0;

const callBackFunction = (accumulator, currentValue,index,array) => {
  return accumulator + currentValue;
};

const result = testReduceFunction(testArray, callBackFunction);

console.log(result);
