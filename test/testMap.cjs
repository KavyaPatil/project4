const testArray = require("../array.cjs");
const testMapFunction = require("../map.cjs");

const callBackFunction = (item, index) => {
  return item + item;
};

const result = testMapFunction(testArray, callBackFunction);

console.log(result);
