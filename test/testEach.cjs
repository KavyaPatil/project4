const testArray = require("../array.cjs");
const testEachFunction = require("../each.cjs");

const callBackFunction = (item, index) => {
  console.log(item, index);
};

const result = testEachFunction(testArray, callBackFunction);

console.log(result);
