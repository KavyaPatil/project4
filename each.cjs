const eachFunction = (array, cbfunction) => {
  for (let index = 0; index < array.length; index++) {
    cbfunction(array[index], index,array);
  }
};

module.exports = eachFunction;
