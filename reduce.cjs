const reduceFunction = (array, cbFunction, initialValue) => {
  if (initialValue === undefined) {
    initialValue = array[0];
    for (let index = 1; index < array.length; index++) {
      initialValue = cbFunction(initialValue, array[index], index, array);
    }
  } else {
    for (let index = 0; index < array.length; index++) {
      initialValue = cbFunction(initialValue, array[index], index, array);
    }
  }

  return initialValue;
};

module.exports = reduceFunction;
