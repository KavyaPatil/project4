const mapFunction = (array, cbFunction) => {
  const returnedArray = [];
  for (let index = 0; index < array.length; index++) {
    returnedArray[index] = cbFunction(array[index], index,array);
    //console.log(cbFunction(array[index], index));
  }
  //console.log(returnedArray);
  return returnedArray;
};

module.exports = mapFunction;
